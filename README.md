# Android Fundamentals (Activities)

This kata is about some Android fundamental concepts. You will learn how to create a project, add some Activities, and navigate between them. You will also learn concepts like Intents, Manifest, project structure and some good practices.

## **Step 1**: Create your **Android** project

* Open `**Android Studio** (Last version recommended, currently in **4.0.1**)

![](./img/android-studio-create-project.png)

```
If you have the Android Studio already running, create a New Project by selecting File -> New -> New Project.
```

* Select the first option: Start a new Android Studio Project

* Select **No Activity** template and press **Next**

* Add a name and a package-name to your project, select **Kotlin** and **minSdk** 21. Then press **Create**

* Inspect the Android Studio Project and discuss with your buddy to discover what are the folders and files for.

```
It's suggested to use the Project View to see the "unformatted" directory.
```

## **Step 2**: Try to run the App

Press the run button in Android Studio and discuss what happened and why.

```
An Activity represents a single screen in your app with which your user can perform a single, focused task such as taking a photo, sending an email, or viewing a map. An activity is usually presented to the user as a full-screen window.
Each time a new activity starts, the previous activity is stopped, but the system preserves the activity in a stack (the "back stack"). When a new activity starts, that new activity is pushed onto the back stack and takes user focus. The back stack follows basic "last in, first out" stack logic. When the user is done with the current activity and presses the Back button, that activity is popped from the stack and destroyed, and the previous activity resumes.
```

## **Step 3**: Run the Project

Add a default Activity and Run the Program. To achieve it, create a Kotlin file and call it `MainActivity.kt` (It's just an arbitrary name) inside the package name, into the main folder as the image below shows:

![](./img/main-activity-place.png)

* The activity should extend form Activity on Android. We'll use the `AppCompatActivity`

* Add the code in the `AndroidManifest.xml` to set the activity as an entry point for your app:

``` xml
<application>
    ...

    <activity android:name=".MainActivity">
        <intent-filter>
            <action android:name="android.intent.action.MAIN" />
            <category android:name="android.intent.category.LAUNCHER" />
        </intent-filter>
    </activity>

    ...
</application>
```
## **Step 4**: Run the Project (finally)

Run the project and see the app running on a Device ( if you don't have an android emulator created, see the Create Emulator Step)

## **Step 5**: Add content to your Activity

Our project is about to communicate two differents activities by sending a message from the `LAUNCHER` activity to a second activity that we should start "manually".

But first, let's add some `Widget`s to the activity to send a message.

* Create a layout file under `res/layout`. Use the `ConstraintLayout` base component ( *add dependency* )

* Assign the `layout` file to your recent activity by overriding the `onCreate()` method and add the following code after `super.onCreate()`:

``` kotlin
override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
}
```

Now you can add the widgets to your `MainActivity`. We are going to send a message to the second activity, so we need an input field and a button:

```xml
<androidx.appcompat.widget.AppCompatEditText
        android:id="@+id/message_input_field"
        android:layout_width="0dp"
        android:layout_height="50dp"
        android:hint="Enter your message here"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toStartOf="@id/send_message_button"
        app:layout_constraintStart_toStartOf="parent" />

    <androidx.appcompat.widget.AppCompatButton
        android:id="@+id/send_message_button"
        android:layout_width="wrap_content"
        android:layout_height="0dp"
        android:text="Send"
        app:layout_constraintBottom_toBottomOf="@id/message_input_field"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintTop_toTopOf="@id/message_input_field" />
```

* Run the project again and see the main activity is now rendering your layout.

## **Step 6**: Prepare your activity to communicate with the second one

We added the needed widgets to send a message but, those widgets do nothing yet. The first thing to do to interact with those widgets is to get a reference of the widgets in our activity. There are several ways to achieve that, we will use the standard option: `findViewById`

Create the fields in the `MainActivity` class:

``` kotlin
private lateinit var messageInput: AppCompatEditText
private lateinit var sendMessageButton: AppCompatButton
```

And add the following code after the `setContentView` call in `onCreate()`:

``` kotlin
messageInput = findViewById(R.id.message_input_field)
sendMessageButton = findViewById(R.id.send_message_button)
```

**What happend if we put the code before `setContentView()`?** 

Try it and discuss with your buddy what happened.

After getting an instance of the widgets, let's add a callback to "receive" on clicks from the `AppCompatButton` widget. Add the code below `sendMessageButton = findViewById(R.id.send_message_button)`.

```kotlin
sendMessageButton.setOnClickListener {
    Toast.makeText(this, "Sending Message", Toast.LENGTH_SHORT).show()
}
```

## **Step 7**: Create a Second Activity

```
An activity is started or activated with an intent. An Intent is an asynchronous message that you can use in your activity to request an action from another activity, or from some other app component. You use an intent to start one activity from another activity, and to pass data between activities.

An Intent can be explicit or implicit:

An explicit intent is one in which you know the target of that intent. That is, you already know the fully qualified class name of that specific activity.
An implicit intent is one in which you do not have the name of the target component, but you have a general action to perform.
In this practical, you create explicit intents. You find out how to use implicit intents in a later practical.
```

Let's create a second `Activity` to send our message to.
* Create in the same directory as the `MainActivity` another file called `MessageDisplayActivity` and make it extend from `AppCompatActivity`.

* Replace the `Toast` with the following code to navigate to the second activity. We won't see any message but, at least we'll navigate to an empty view:

```kotlin 
val intent = Intent(this, MessageDisplayActivity::class.java)
startActivity(intent)
```

**What happened?**

All the activities in our Application must be declared in the `AndroidManifest` file in order to be seen by the system. Otherwise, we won't be able to call them. Let's add the new activity in the manifest file by with the following code inside the `<application/>` tag:

```kotlin 
<activity android:name=".MessageDisplayActivity" />
```

* Run the app again and verify that you can navigate now.

## **Step 8**: Send and Receive Message

Our next step is to send a message from the `MainActivity` to the `MessageDisplayActivity`. To achieve it, we must add some code in both activities. In one `Activity` we'll add the message into the `Intent` which will be sent to the second `Activity`; which will also need the code to read from the `Intent` received.

* Create a `layout` file under the `res/layout` folder (just the way we did with the main activity and find an appropiete name for it).

* Add the following code to the the `xml` file:

```xml
<androidx.appcompat.widget.AppCompatTextView
        android:id="@+id/message_label"
        android:text="The received message is:"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:textAllCaps="true"
        android:textStyle="bold"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"/>

<androidx.appcompat.widget.AppCompatTextView
    android:id="@+id/received_message"
    android:layout_width="0dp"
    android:layout_height="wrap_content"
    app:layout_constraintTop_toBottomOf="@id/message_label"
    app:layout_constraintStart_toStartOf="parent"
    app:layout_constraintEnd_toEndOf="parent"/>
```

* In the `MessageDisplayActivity` add the necessary code to get a `received_message` reference. We will set the received message there.

```
Your intent object can pass data to the target activity in two ways: in the data field, or in the intent extras. The intent data is a URI indicating the specific data to be acted on. If the information you want to pass to an `Activity` through an `Intent` is not a `URI`, or you have more than one piece of information you want to send, you can put that additional information into the extras instead.

The intent extras are key/value pairs in a Bundle. A Bundle is a collection of data, stored as key/value pairs. To pass information from one activity to another, you put keys and values into the intent extra Bundle from the sending activity, and then get them back out again in the receiving activity.
```

We need to get from the received `Intent` the message sent in the previous `Activity`, to achieve that, we have to obtain the `Intent` and extract the `Extras` from the `Bundle`.

* Add the following code after the `setContentView()` in the `MessageDisplayActivity`:

```kotlin
intent.extras?.let { extras ->
    val receivedMessage = extras.getString("MESSAGE_KEY")
    messageReceivedWidget.text = receivedMessage
}
```
Where `messageReceivedWidget` is the name of the field where we stored the `AppCompatTextView` reference.

* To send the message to this `Activity`, we have to create a `Bundle` object and set it as an `Extra` bundle in the `Intent`. Modify the code executed in the Send Button's `onClickListener` lambda to look like this:

```kotlin
val intent = Intent(this, MessageDisplayActivity::class.java)
val message = messageInput.text.toString()
val bundle = Bundle()
bundle.putString("MESSAGE_KEY", message)
intent.putExtras(bundle)
startActivity(intent)
```

Let's run our app again and start sending messages.

**Can you find a problem with this approach?**

Discuss with your buddy what are the problems this approach could have and other possible ways to send the message.

## **Step 9**: Sending objects

Besides the possibility of sending some Primitive Objects (like String, Int) we can also send some custom objects through an `Intent`. This is useful when your domain requires you to send more information grouped into an object, intead of having several primitives values with different keys.

We can achieve this by sending `Serializable` objects.

* Create the `Message` class with a value field and change the code in the `startActivity` to look like this:

```kotlin
val intent = Intent(this, MessageDisplayActivity::class.java)
val message = Message(value = messageInput.text.toString())
val bundle = Bundle()
bundle.putSerializable("MESSAGE_KEY", message)
intent.putExtras(bundle)
startActivity(intent)
```

And the code in the `MessageDisplayActivity` like this:

```kotlin
intent.extras?.let { extras ->
    val receivedMessage = extras.getSerializable("MESSAGE_KEY") as Message
    messageReceivedWidget.text = receivedMessage.value
}
```

* Run the project and verify that everything is still working. The difference is you can add more information in that message without adding any other `Extra` to the bundle.

```
"sniff sniff"... something is not smelling very well here. **What could it be?**
```

Take a moment to recognize the key that we used to send the message. It should match the key used to receive that message. This is a code smell. It's possible that someone try to send a message using a different key or using a key with a typo.

* Change the key when you save or receive your `Message` object to verify it.

* Solve the problem by unifying the key under a static field `String`. This is a way to be sure the key used for sending the message is the same used when received the message. Let's add some code in the `MessageDisplayActivity` which should be the responsible for defining how to receive messages (to define its protocol):

```kotlin
companion object {
    const val MESSAGE_KEY = "MESSAGE_KEY"
}
```

* Replace all the "hardcoded" keys for this static field. Yeah! Now we can send messages with a higher probability of success... Yeah?.. Sure?

* Create a `RichTextMessage` class with a `String` field called `value`, make it implement the `Serializable` interface and send a message with that object instead of `Message`. Verify it compiles, run the application and try it.

![](./img/crash.gif)

As you could verify, there is still something wrong with this approach. Implementing the `Serializable` protocol is not enough to ensure nobody will send an object to the `MessageDisplayActivity` which does not expect.

Discuss which could be a better solutions for this. **SPOILERS** in the next line!

* Create a factory method (preferable in the `MessageDisplayActivity` as an `static` function) to receive a typed parameter with the expected object and return a complete `Intent` which will navigate to the `Activity`. You will also need to send the `Context`:

```kotlin
fun newInstance(context: Context, message: Message): Intent {
    val bundle = Bundle()
    bundle.putSerializable(MESSAGE_KEY, message)
    val intent = Intent(context, MessageDisplayActivity::class.java)
    intent.putExtras(bundle)
    return intent
}
```

Discuss this approach with your buddy!

## Final thoughts

We have sent a message using a factory method to avoid `MessageDisplayActivity` users to make a possible mistake. However, this approach is just an explicit contract we have defined; we cannot be sure nobody will try to communicate with that Activity using that factory method, that's why we need to be more prepared. 

Expecting a `Message` and asumming that the object is going to be passed, no matter what, is error prone. So, it will be better if we have some kind of mecanism to detect if the information we need to achieve our goals (display a message on the screen) is missing and display some kind of error, throw an exception or close the Activity. 

It's not about being *defensive*, it's about being in control. 

Happy Coding! :heart:

## Help

### Add the ConstrintLayout dependency:

``` gradle
implementation "androidx.constraintlayout:constraintlayout:1.1.3"
```

### Create Emulator

* Find the the icon below in the Android Studio.

![](./img/create-emulator.png)

* Press the **Create Virtual Device** and follow the Steps to select an appropiated configuration (ask your body for help).

Author: [Jorge Nogueiras](https://gitlab.com/jorge.nogueiras)

Feedback will be appreciated [here](mailto:jorge.nogueiras@etermax.com)
